/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.m_counter;

/**
 *
 * @author Acer
 */
public class mainCounter {
    public static void main(String[] args) {
        //Reference Type 
        mCounter counter1 = new mCounter();
        mCounter counter2 = new mCounter();
        mCounter counter3 = new mCounter();
        mCounter counter4 = new mCounter();
        //counter1.countNum = 1;//
        counter1.increase(); //counter1.countNum++; >>1
        counter1.increase(); //counter1.countNum++; >>2
        counter1.decrease();
        counter1.print();
        counter2.print();
        counter2.increase();
        counter2.print();
        counter2.decrease();
        counter2.print();
        counter1.increase();
        counter3.increase();
        counter4.decrease();
        counter4.print();
        counter3.increase();
        counter4.increase();
        counter4.increase();
        System.out.println("Show All Counter : ");
        counter1.print();
        counter2.print();
        counter3.print();
        counter4.print();
    }
}
